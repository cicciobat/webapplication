import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  mainText!: string;
  secondText!: string;
  quarantaquattro: number = 44;
  uno: number = 1;
  counter: number[] = [10, parseInt('23', 10), this.quarantaquattro, 57, this.uno, parseInt('2', 10)];


}
